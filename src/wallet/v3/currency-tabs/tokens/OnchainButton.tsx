import { observer } from 'mobx-react';
import React from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MText from '../../../../common/components/MText';

import SegmentedButton from '../../../../common/components/SegmentedButton';
import i18n from '../../../../common/services/i18n.service';
import ThemedStyles from '../../../../styles/ThemedStyles';
import { WalletStoreType } from '../../../v2/createWalletStore';
import {
  UniqueOnChainStoreType,
  isConnected as isWalletConnected,
} from '../../useUniqueOnchain';

type PropsType = {
  containerStyle?: ViewStyle | Array<ViewStyle>;
  onPress?: () => void;
  walletStore: WalletStoreType;
  onchainStore: UniqueOnChainStoreType;
  style?: StyleProp<ViewStyle>;
};

const OnchainButton = observer((props: PropsType) => {
  const theme = ThemedStyles.style;
  const textStyles = [theme.colorPrimaryText, theme.fontM, theme.fontMedium];
  const hasReceiver =
    props.walletStore.wallet.receiver &&
    props.walletStore.wallet.receiver.address;

  const isConnected = isWalletConnected(props.onchainStore);

  const children: any = {};

  children.childrenButton1 = (
    <MText style={[textStyles]}>
      {props.walletStore.wallet.eth.balance} ETH
    </MText>
  );

  children.childrenButton2 = !isConnected ? (
    <MText style={textStyles}>
      <MText style={[theme.colorSecondaryText, theme.fontMedium]}>
        {i18n.t(hasReceiver ? 'wallet.reconnect' : 'wallet.connect') + ' '}
        <Icon name="plus-circle" size={15} style={theme.colorPrimaryText} />
      </MText>{' '}
    </MText>
  ) : (
    <MText style={textStyles}>
      <MText style={[theme.colorSecondaryText, theme.fontMedium]}>
        {props.walletStore.wallet.receiver.address?.substr(0, 4)}...
        {props.walletStore.wallet.receiver.address?.substr(-4)}
      </MText>
    </MText>
  );

  return props.onchainStore.loading ? (
    <View>
      <MText style={[props.style, theme.bgPrimaryBorder, theme.marginLeft2x]}>
        ...
      </MText>
    </View>
  ) : (
    <SegmentedButton
      containerStyle={props.containerStyle}
      {...children}
      onPress={props.onPress}
    />
  );
});

export default OnchainButton;
