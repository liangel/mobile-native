import React from 'react';
import { View, Image, StyleSheet, Platform } from 'react-native';

import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { IconButton, Icon } from '~ui/icons';

import { ListItem } from 'react-native-elements';

import i18n from '../common/services/i18n.service';
import ThemedStyles from '../styles/ThemedStyles';
import featuresService from '../common/services/features.service';
import sessionService from '../common/services/session.service';
import FitScrollView from '../common/components/FitScrollView';
import requirePhoneValidation from '../common/hooks/requirePhoneValidation';
import MText from '../common/components/MText';

const getOptionsList = navigation => {
  const hasRewards = sessionService.getUser().rewards;

  let list = [
    {
      name: i18n.t('newsfeed.title'),
      icon: <Icon name="home" />,
      onPress: () => {
        navigation.navigate('Newsfeed');
      },
    },
    {
      name: i18n.t('discovery.title'),
      icon: <Icon name="hashtag" />,
      onPress: () => {
        navigation.navigate('Discovery');
      },
    },
    featuresService.has('plus-2020')
      ? {
          name: i18n.t('wire.lock.plus'),
          icon: <Icon name="queue" />,
          onPress: () => {
            navigation.navigate('Tabs', {
              screen: 'CaptureTab',
              params: { screen: 'PlusDiscoveryScreen' },
            });
          },
        }
      : null,
    featuresService.has('crypto')
      ? {
          name: i18n.t('moreScreen.wallet'),
          icon: <Icon name="bank" />,
          onPress: () => {
            navigation.navigate('Tabs', {
              screen: 'CaptureTab',
              params: { screen: 'Wallet' },
            });
          },
        }
      : null,
    {
      name: i18n.t('earnScreen.title'),
      icon: <Icon name="money" />,
      onPress: () => {
        navigation.navigate('EarnModal');
      },
    },
    {
      name: 'Buy Tokens',
      icon: <Icon name="coins" />,
      onPress: async () => {
        const navToBuyTokens = () => {
          navigation.navigate('Tabs', {
            screen: 'CaptureTab',
            params: { screen: 'BuyTokens' },
          });
        };
        if (!hasRewards) {
          await requirePhoneValidation();
          navToBuyTokens();
        } else {
          navToBuyTokens();
        }
      },
    },
  ];
  list = [
    ...list,
    {
      name: 'Analytics',
      icon: <Icon name="analytics" />,

      onPress: () => {
        navigation.navigate('Tabs', {
          screen: 'CaptureTab',
          params: { screen: 'Analytics' },
        });
      },
    },
    {
      name: i18n.t('discovery.groups'),
      icon: <Icon name="group" />,
      onPress: () => {
        navigation.navigate('Tabs', {
          screen: 'CaptureTab',
          params: { screen: 'GroupsList' },
        });
      },
    },
    {
      name: i18n.t('moreScreen.settings'),
      icon: <Icon name="settings" />,
      onPress: () => {
        navigation.navigate('Tabs', {
          screen: 'CaptureTab',
          params: { screen: 'Settings' },
        });
      },
    },
  ];

  return list;
};

/**
 * Drawer menu
 * @param props
 */
export default function Drawer(props) {
  const channel = sessionService.getUser();

  const navToChannel = () => {
    props.navigation.closeDrawer();
    props.navigation.push('Channel', { entity: channel });
  };

  const avatar =
    channel && channel.getAvatarSource ? channel.getAvatarSource('medium') : {};

  const optionsList = getOptionsList(props.navigation);
  return (
    <SafeAreaView style={containerStyle}>
      <FitScrollView style={containerStyle}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={navToChannel}>
            <Image source={avatar} style={styles.wrappedAvatar} />
          </TouchableOpacity>
          <View style={styles.row}>
            <View style={titleContainerStyle}>
              <MText style={titleStyle} onPress={navToChannel}>
                {channel.name || `@${channel.username}`}
              </MText>
              {channel.name && (
                <MText
                  onPress={navToChannel}
                  style={subtitleStyle}
                  testID="channelUsername">
                  @{channel.username}
                </MText>
              )}
            </View>
            <IconButton
              scale
              color="SecondaryText"
              name="account-multi"
              testID="multiUserIcon"
              onPress={() => props.navigation.navigate('MultiUserScreen')}
            />
          </View>
        </View>
        <View style={styles.body}>
          {optionsList.map((l, i) =>
            !l ? null : (
              <ListItem
                Component={TouchableOpacity}
                pad={5}
                key={i}
                onPress={l.onPress}
                containerStyle={styles.listItem}>
                {l.icon}
                <ListItem.Content>
                  <ListItem.Title style={menuTitleStyle}>
                    {l.name}
                  </ListItem.Title>
                </ListItem.Content>
              </ListItem>
            ),
          )}
        </View>
      </FitScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  titleText: {
    fontFamily: 'Roboto',
    fontSize: Platform.select({ ios: 26, android: 24 }),
    fontWeight: '700',
  },
  subTitleText: {
    fontFamily: 'Roboto',
    fontSize: Platform.select({ ios: 16, android: 15 }),
    fontWeight: '400',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingTop: Platform.select({ ios: 33, android: 23 }),
    paddingLeft: 40,
    paddingBottom: 25,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#CCC',
  },
  icon: {
    width: 30,
  },
  menuText: {
    fontSize: Platform.select({ ios: 21, android: 19 }),
    fontWeight: '700',
    paddingLeft: 10,
  },
  wrappedAvatar: {
    height: 55,
    width: 55,
    borderRadius: 55,
  },
  body: {
    paddingLeft: Platform.select({ ios: 35, android: 25 }),
    paddingTop: Platform.select({ ios: 50, android: 30 }),
  },
  container: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    paddingLeft: 30,
  },
  listItem: {
    borderBottomWidth: 0,
    backgroundColor: 'transparent',
    paddingVertical: Platform.select({ ios: 15, android: 12.5 }),
  },
  row: {
    flexDirection: 'row',
    flex: 1,
    paddingRight: 30,
  },
});
const menuTitleStyle = ThemedStyles.combine(
  styles.menuText,
  'colorPrimaryText',
);
const containerStyle = ThemedStyles.combine(
  'flexContainer',
  'bgPrimaryBackground',
);
const subtitleStyle = ThemedStyles.combine(
  styles.subTitleText,
  'colorSecondaryText',
);
const titleContainerStyle = ThemedStyles.combine('flexColumn', 'marginLeft2x');
const titleStyle = ThemedStyles.combine(styles.titleText, 'colorPrimaryText');
// const iconStyle = ThemedStyles.combine('colorIcon', styles.icon);
